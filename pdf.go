package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/jung-kurt/gofpdf"
)

type Product struct {
	SKU         string `json:"sku"`
	Title       string `json:"title"`
	Description string `json:"description"`
	HowItHelps  string `json:"how_it_helps"`
}

func main() {

	sku := flag.String("sku", "", "Product SKU value")
	hash := flag.String("hash", "", "Unique hash of order item")
	fromName := flag.String("from", "", "Card sender")
	toName := flag.String("to", "", "Card recipient")
	message := flag.String("message", "", "Card message")
	assetpath := flag.String("assetpath", "", "File path where assets live")
	outputpath := flag.String("outputpath", "", "File path pdfs get written")

	flag.Parse()

	// make sure flags are set
	if *sku == "" {
		fmt.Println("No sku supplied!")
		os.Exit(0)
	}

	if *hash == "" {
		fmt.Println("No order hash supplied!")
		os.Exit(0)
	}

	if *message == "" {
		fmt.Println("No message supplied!")
		os.Exit(0)
	}

	if *toName == "" {
		fmt.Println("No to name supplied!")
		os.Exit(0)
	}

	if *fromName == "" {
		fmt.Println("No from name supplied!")
		os.Exit(0)
	}

	if *assetpath == "" {
		fmt.Println("No asset path supplied!")
		os.Exit(0)
	}

	if *outputpath == "" {
		fmt.Println("No output path supplied!")
		os.Exit(0)
	}

	productjson, err := ioutil.ReadFile(*assetpath + "/wishlistdata.json")
	if err != nil {
		fmt.Println("err: ", err)
	}

	var Products []Product

	err = json.Unmarshal(productjson, &Products)
	if err != nil {
		fmt.Println("error:", err)
		os.Exit(0)
	}

	var p Product

	for i := range Products {
		if Products[i].SKU == *sku {
			p = Products[i]
		}
	}

	// product details are set from sku
	if p.Title == "" {
		fmt.Println("No matching sku found")
		os.Exit(0)
	}

	var hih = strings.Split(p.HowItHelps, "\n\n")

	pdf := gofpdf.New("P", "mm", "A4", "")

	// Page 1
	pdf.AddPage()

	// set margins
	pdf.SetMargins(0, 0, 0)

	// add the fonts
	pdf.SetFontLocation(*assetpath + "/fonts")
	pdf.AddFont("stc", "", "stc.json")
	pdf.AddFont("gillsans", "", "gillsans.json")
	pdf.AddFont("gill-sans-bold", "", "gill-sans-bold.json")

	// add main header
	pdf.SetFont("stc", "", 236)
	pdf.SetTextColor(35, 31, 32)
	pdf.SetXY(28, 31)
	pdf.CellFormat(151, 78, "a", "0", 1, "L", false, 0, "") // a is the main header glyph

	// draw gery rectangle
	pdf.SetFillColor(243, 243, 244)
	pdf.SetXY(0, 118)
	pdf.CellFormat(210, 95, "", "0", 0, "", true, 0, "")

	// add product image
	pdf.Image(*assetpath+"/page1/"+*sku+".png", 0, 109, 210, 124, false, "", 0, "")

	// to text
	pdf.SetXY(30, 120)
	pdf.SetFont("gillsans", "", 14)
	pdf.CellFormat(130, 20, "To:", "0", 1, "L", false, 0, "")
	pdf.SetXY(39, 120)
	pdf.CellFormat(132, 20, *toName, "0", 1, "L", false, 0, "")

	// message
	pdf.SetX(30)
	pdf.SetFont("gillsans", "", 12)
	pdf.MultiCell(65, 5, *message, "0", "L", false)

	// from text
	pdf.SetXY(30, 190)
	pdf.SetFont("gillsans", "", 14)
	pdf.CellFormat(130, 20, "From:", "0", 1, "L", false, 0, "")
	pdf.SetXY(44, 190)
	pdf.CellFormat(137, 20, *fromName, "0", 1, "L", false, 0, "")

	// product title
	pdf.SetXY(30, 230)
	pdf.SetFont("gillsans", "", 22)
	pdf.SetTextColor(110, 109, 113)
	pdf.CellFormat(130, 6, p.Title, "0", 1, "L", false, 0, "")

	// product description
	pdf.SetXY(30, 240)
	pdf.SetFontSize(11)
	pdf.MultiCell(150, 5, p.Description, "0", "L", false)

	// footer
	pdf.SetXY(16, 268)
	pdf.SetTextColor(35, 31, 32)
	pdf.SetFont("stc", "", 24)
	pdf.CellFormat(30, 8, "b", "0", 1, "L", false, 0, "") // b is the wishlist logo glyph

	pdf.SetTextColor(110, 109, 113)
	pdf.SetFont("gillsans", "", 7)
	pdf.Text(5, 280, "Wishlist by Save the Children is a range of inspirational gifts for friends and family that save the lives of children around")
	pdf.Text(5, 283, "the world and help them fulfil their potential. Make a wish come true now at")

	pdf.SetTextColor(110, 109, 113)
	pdf.SetFont("gillsans", "", 5)
	pdf.Text(5, 289, "Every Wishlist gift belongs to a theme covering different types of work we do. When a")
	pdf.Text(5, 291, "gift is bought for you, the money will contribute to projects related to that gift's theme.")

	pdf.SetFontSize(6)
	pdf.Text(190, 281, "1 St John's Lane")
	pdf.Text(187, 283.4, "London EC1M 4AR")
	pdf.Text(182, 285.8, "Tel: +44 (0)20 7012 6400")

	pdf.SetFontSize(5)
	pdf.Text(155, 292, "Registered charity England and Wales (213890) Scotland (SC039570)")

	pdf.SetFont("Gill-Sans-Bold", "", 8)
	pdf.SetTextColor(35, 31, 32)
	pdf.Text(82.4, 283, "savethechildren.org.uk/wishlist")

	pdf.SetFontSize(7)
	pdf.Text(178, 289, "savethechildren.org.uk")

	pdf.SetFont("stc", "", 11)
	pdf.Text(176, 276, "d") // d is the stc logo text glyph
	pdf.SetTextColor(237, 28, 36)
	pdf.SetFontSize(24)
	pdf.Text(167, 278, "c") // c is the stc child character glyph

	// Page 2
	pdf.AddPage()

	// add product image
	pdf.Image(*assetpath+"/page2/"+*sku+".jpg", 35, 0, 175, 297, false, "", 0, "")

	// how this gift helps copy
	pdf.SetTextColor(110, 109, 113)
	pdf.SetFont("gillsans", "", 11)
	pdf.TransformBegin()
	pdf.TransformRotate(270, 30.5, 5)
	pdf.SetXY(30, 5)
	pdf.MultiCell(90, 4.5, hih[0], "0", "L", false)
	pdf.TransformEnd()

	// how this gift helps 2nd column copy
	if len(hih) == 2 {
		pdf.SetXY(30.5, 100)
		pdf.TransformBegin()
		pdf.TransformRotate(270, 30.5, 100)
		pdf.MultiCell(90, 4.5, hih[1], "0", "L", false)
		pdf.TransformEnd()
	}

	// add wishlist logo
	pdf.SetTextColor(35, 31, 32)
	pdf.SetXY(14, 269.9)
	pdf.SetFont("stc", "", 32)
	pdf.TransformBegin()
	pdf.TransformRotate(270, 14, 269.9)
	pdf.CellFormat(7, 7, "b", "0", 1, "L", false, 0, "") // b is the wishlist logo glyph
	pdf.TransformEnd()

	// write the file to disk
	err = pdf.OutputFileAndClose(*outputpath + "/wishlist-" + *hash + ".pdf")
	if err != nil {
		fmt.Println("Err: ", err)
	} else {
		fmt.Println("File successfully written: " + *outputpath + "/wishlist-" + *hash + ".pdf")
	}

}
